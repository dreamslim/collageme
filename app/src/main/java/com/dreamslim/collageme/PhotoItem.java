package com.dreamslim.collageme;

import android.widget.ImageView;

public class PhotoItem {

    public ImageView photo;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
