package com.dreamslim.collageme;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PhotoPickerActivity extends Activity {

    private static final String LOG_TAG = PhotoPickerActivity.class.getSimpleName();
    private ArrayList<PhotoItem> photoItems = new ArrayList<>();
    private List<PhotoItem> selectedImages;
    private PhotoAdapter adapter;
    private String nickName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_picker);

        Bundle bundle = getIntent().getExtras();
        String[] photos = bundle.getStringArray("photo");
        nickName = bundle.getString("nickname");

        PhotoItem photoItem;
        if (photos != null) {
            for (String photo : photos) {
                ImageView imageView = new ImageView(this);
                imageView.setImageBitmap(loadImage(photo));
                photoItem = new PhotoItem();
                photoItem.photo = imageView;
                photoItem.setSelected(false);
                photoItems.add(photoItem);
            }
        }
        else {
            Log.d(LOG_TAG, "Get photo from Main error");
            return;
        }

        ListView photosListView = (ListView) findViewById(R.id.photos_list);
        selectedImages = new ArrayList<>();

        adapter = new PhotoAdapter(this, R.layout.item, photoItems.toArray(new PhotoItem[photoItems.size()]));
        photosListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        photosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.toggleSelection(position);
                view.setBackgroundColor(adapter.getItem(position).isSelected() ? Color.RED : Color.TRANSPARENT);
            }
        });
        photosListView.setAdapter(adapter);
    }

    public void buildCollageClick(View view) {
        selectedImages = adapter.getSelectedItems();
        if (selectedImages.size() < 2) {
            Toast.makeText(this, "Выберите хотя бы две фотографии!", Toast.LENGTH_SHORT).show();
            return;
        }

        InternalStorage internalStorage = new InternalStorage();
        Bitmap collage = createCollageFromImages(selectedImages);
        String collageDirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CollageMe/images/" + nickName + "/collage";
        String collagePath = internalStorage.saveImage(collage, collageDirPath, "collage");
        Intent intent = new Intent(this, CollageSenderActivity.class);
        intent.putExtra("collage_path", collagePath);
        startActivity(intent);
    }

    private Bitmap createCollageFromImages(List<PhotoItem> selectedImages) {
        Bitmap bitmap = null;
        Canvas canvas;
        int photoCount = selectedImages.size();
        int COLUMNS = 2;
        int ROWS = 2;
        int width = ((BitmapDrawable) selectedImages.get(0).photo.getDrawable()).getBitmap().getWidth();
        int height = ((BitmapDrawable) selectedImages.get(0).photo.getDrawable()).getBitmap().getHeight();

        try {
            if (photoCount % 2 == 0) {
                ROWS = photoCount / COLUMNS;

                bitmap = Bitmap.createBitmap(width * COLUMNS, height * ROWS, Bitmap.Config.ARGB_8888);
                canvas = new Canvas(bitmap);
                int count = 0;
                Bitmap currentBitmap;
                for (int rows = 0; rows < ROWS; rows++) {
                    for (int cols = 0; cols < COLUMNS; cols++) {
                        currentBitmap = ((BitmapDrawable) selectedImages.get(count).photo.getDrawable()).getBitmap();
                        canvas.drawBitmap(currentBitmap, width * cols, height * rows, null);
                        count++;
                    }
                }
            } else {
                photoCount--;
                int scale = 2;
                COLUMNS = photoCount / ROWS;

                Bitmap firstBitmap = ((BitmapDrawable) selectedImages.get(0).photo.getDrawable()).getBitmap();
                Bitmap rightBitmap = Bitmap.createBitmap(width * COLUMNS, height * ROWS, Bitmap.Config.ARGB_8888);
                canvas = new Canvas(rightBitmap);
                int count = 1;
                Bitmap currentBitmap;
                for (int rows = 0; rows < ROWS; rows++) {
                    for (int cols = 0; cols < COLUMNS; cols++) {
                        currentBitmap = ((BitmapDrawable) selectedImages.get(count).photo.getDrawable()).getBitmap();
                        canvas.drawBitmap(currentBitmap, width * cols, height * rows, null);
                        count++;
                    }
                }

                Bitmap leftBitmap = Bitmap.createScaledBitmap(firstBitmap, width * scale, height * scale, true);
                bitmap = Bitmap.createBitmap(leftBitmap.getWidth() + rightBitmap.getWidth(),
                        leftBitmap.getHeight(), Bitmap.Config.ARGB_8888);
                canvas = new Canvas(bitmap);

                canvas.drawBitmap(leftBitmap, 0, 0, null);
                canvas.drawBitmap(rightBitmap, leftBitmap.getWidth(), 0, null);
            }
        } catch (Exception e) {
            Log.v(LOG_TAG, e.toString());
        }
        return bitmap;
    }

    private Bitmap loadImage(String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                return null;
            }
            InputStream inputStream = new FileInputStream(file);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
