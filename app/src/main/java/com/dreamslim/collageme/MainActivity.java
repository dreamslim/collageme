package com.dreamslim.collageme;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private static final String CLIENT_ID = "3c1e20ba1ae246e0b7bc2c602b72d6ee";
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private EditText nickNameEdit;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nickNameEdit = (EditText) findViewById(R.id.nickname);
        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Идёт загрузка фотографий...");
    }

    public void onCollageCreate(View view) {
        String nickName = nickNameEdit.getText().toString();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(nickNameEdit.getWindowToken(), 0);

        if (nickName.isEmpty()) {
            Toast.makeText(this, "Введите никнейм!", Toast.LENGTH_SHORT).show();
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                RetrieveDataTask task = new RetrieveDataTask();
                task.execute(nickName);
            } else {
                Toast.makeText(this, "Подключение к сети не установлено!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class RetrieveDataTask extends AsyncTask<String, Void, List<String>> {
        List<String> photoUrls;
        List<String> photos = new ArrayList<>();
        private String nickName;
        final String DATA = "data";
        final String ID = "id";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected List<String> doInBackground(String... params) {
            nickName = params[0];

            try {
                String userIdRequest = "https://api.instagram.com/v1/users/search?q=" + nickName + "&client_id=" + CLIENT_ID;
                String response = getJsonData(userIdRequest);

                if (response == null)
                    return null;

                long userId = getUserIdFromJson(response);

                if (userId == 0)
                    return null;

                String photoUrlRequest = "https://api.instagram.com/v1/users/" + userId + "/media/recent/?client_id=" + CLIENT_ID;
                String photoUrlResponse = getJsonData(photoUrlRequest);

                if (photoUrlResponse == null)
                    return null;

                photoUrls = getPhotoUrlsFromJson(photoUrlResponse);

                for (int i = 0; i < photoUrls.size(); i++) {
                    try {
                        InternalStorage internalStorage = new InternalStorage();
                        Bitmap bmp = Picasso.with(getApplicationContext()).load(photoUrls.get(i)).get();
                        String imageDirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CollageMe/images/" + nickName;
                        photos.add(internalStorage.saveImage(bmp, imageDirPath, "photo" + i));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return photos;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<String> urls) {
            super.onPostExecute(urls);
            mProgressDialog.cancel();
            if (urls == null) {
                Toast.makeText(MainActivity.this, "По указанному никнейму нет данных!", Toast.LENGTH_SHORT).show();
            } else {
                if (urls.size() != 0) {
                    Intent intent = new Intent(getApplicationContext(), PhotoPickerActivity.class);
                    intent.putExtra("photo", urls.toArray(new String[urls.size()]));
                    intent.putExtra("nickname", nickNameEdit.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "У данного пользователя нет фотографий!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        private String getJsonData(String request) {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String dataJsonString = null;
            String line;

            try {
                URL url = new URL(request);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                if (inputStream == null)
                    return null;

                StringBuilder buffer = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }

                if (buffer.length() != 0)
                    dataJsonString = buffer.toString();

                return dataJsonString;
            } catch (MalformedURLException e) {
                Log.e(LOG_TAG, "URL error ", e);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Connection error ", e);
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.e(LOG_TAG, "Closing stream error ", e);
                    }
                }
            }
            return null;
        }

        private long getUserIdFromJson(String jsonStr) throws JSONException {
            JSONObject dataJson = new JSONObject(jsonStr);
            JSONArray dataJsonArray = dataJson.getJSONArray(DATA);

            if (isUserExist(dataJsonArray, nickName)) {
                JSONObject object = dataJsonArray.getJSONObject(0);
                return object.getLong(ID);
            } else
                return 0;
        }

        private List<String> getPhotoUrlsFromJson(String jsonStr) throws JSONException {
            final String IMAGES = "images";
            final String RESOLUTION = "standard_resolution";
            final String URL = "url";
            List<String> urls = new ArrayList<>();
            JSONArray jsonArray = new JSONObject(jsonStr).getJSONArray(DATA);
            String url;

            for (int i = 0; i < jsonArray.length(); ++i) {
                url = jsonArray.getJSONObject(i).getJSONObject(IMAGES).getJSONObject(RESOLUTION).get(URL).toString();
                urls.add(url);
            }
            return urls;
        }

        private boolean isUserExist(JSONArray array, String username) {
            return array.toString().contains("\"username\":\"" + username + "\"");
        }
    }
}
