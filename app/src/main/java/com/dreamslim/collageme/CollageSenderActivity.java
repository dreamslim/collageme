package com.dreamslim.collageme;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

public class CollageSenderActivity extends Activity {

    private static final String LOG_TAG = CollageSenderActivity.class.getSimpleName();
    private static String EMAIL;
    private String collagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_sender);
        PreferenceManager.setDefaultValues(this, R.xml.settings, true);

        Bundle data = getIntent().getExtras();
        collagePath = data.getString("collage_path");
        ImageView imageView = (ImageView) findViewById(R.id.previewImage);
        File collage = new File(collagePath);
        Picasso.with(this).load(collage).into(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_collage_sender, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent.setClass(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        EMAIL = settings.getString("email", "dreamslim@gmail.com");
    }

    public void sendEmail(View view) {
        String subject = "Collage from CollageMe";
        String message = "There is your collage";
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{EMAIL});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        Log.d(LOG_TAG, "Path to collage file " + collagePath);
        email.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + collagePath));
        email.setType("message/rfc822");
        try {
            startActivity(Intent.createChooser(email, "Отправить коллаж..."));
        } catch (ActivityNotFoundException exception) {
            Toast.makeText(this, "Нет установленных e-mail клиентов", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        InternalStorage internalStorage = new InternalStorage();
        File imagesDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/CollageMe");
        internalStorage.deleteImage(imagesDir);
    }
}
