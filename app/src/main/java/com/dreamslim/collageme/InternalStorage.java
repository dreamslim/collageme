package com.dreamslim.collageme;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class InternalStorage {

    private static final String LOG_TAG = InternalStorage.class.getSimpleName();

    public String saveImage(Bitmap bitmap, String imagesDirPath, String imageName) {
        FileOutputStream fileOutputStream;
        File imagesDir = new File(imagesDirPath);

        if (!imagesDir.exists())
            if (!imagesDir.mkdirs()) {
                Log.e(LOG_TAG, "Creating image directory error, path " + imagesDir.getAbsolutePath());
                return null;
            }

        File image = new File(imagesDir, imageName + ".jpg");
        if (image.exists())
            if (!image.delete()) {
                Log.e(LOG_TAG, "Image delete error, path " + image.getAbsolutePath());
                return null;
            }

        try {
            fileOutputStream = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image.getAbsolutePath();
    }

    public void deleteImage(File image) {
        if (image.isDirectory()) {
            for (File child : image.listFiles())
                deleteImage(child);
        }
        if (!image.delete()) {
            Log.e(LOG_TAG, "Image delete error, path " + image.getAbsolutePath());
        }
    }
}
